<!-- ~/dox/med/8/MIN-RES/MED-MIN13-notes.md -->

```text
 ████     ████ ██ ████     ██  ██   ████
░██░██   ██░██░██░██░██   ░██ ███  █░░░ █
░██░░██ ██ ░██░██░██░░██  ░██░░██ ░    ░█
░██ ░░███  ░██░██░██ ░░██ ░██ ░██    ███
░██  ░░█   ░██░██░██  ░░██░██ ░██   ░░░ █
░██   ░    ░██░██░██   ░░████ ░██  █   ░█
░██        ░██░██░██    ░░███ ████░ ████
░░         ░░ ░░ ░░      ░░░ ░░░░  ░░░░
```

```text
symbols: ←→↓↑⇄⇋ *∗†‡⁂ €
greek: αβγδΔεκλμσπτρφχ
logic: ∴ ∵
math: − ∓ ∙ √ ∛ ∜ ∞ ∟ ⊾ ∠ ∡ ⊾ ≈ ≙ ≡ ≤ ≥ ∅
```

# MED-MIN13 -- Medical Biotechnology towards Clinical Practice #

## Overview ##

![MIN13 overview](./doc/overview.png)

- [DOC -- Information](https://brightspace.ru.nl/d2l/le/content/85951/Home)
    + Contact information
    + [Q9 Schedule](https://healthacademy.radboudumc.nl/roosters/getrooster.php?style=student&courseid=UMCN-MIN13-k1-1920)
    + Q9 Course Manual
    + [Q10 Schedule](https://healthacademy.radboudumc.nl/roosters/getrooster.php?style=student&directlink=y&studiejaar=1920&periode=k2&isiscode=MIN13)
    + Q10 Course Manual
    + Exam Regulation 1920
        * "NB - This regulation holds for NWI-MOL104 (items Q9; 1-4) and MED-MIN13 (all items)."
    + Exam prep
- [EL -- E-learning Computer Practicals](https://brightspace.ru.nl/d2l/le/content/85951/Home)
    + [Test yourself: Cell Biological & Biochemical Principles and Techniques](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399100/View)
    + 1 - Rec-DNA and Recombinant Proteins (Sep 5, 2019 13:00)
    + 2 - Genetically Modified Organisms (Sep 6, 2019 13:00)
    + [Test yourself: Immunological Principles](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399103/View)
    + [Test yourself: Lab Safety](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399104/View)
    + [-DEADLINE- Biotechniques](https://brightspace.ru.nl/d2l/lms/quizzing/user/quiz_summary.d2l?ou=85951&qi=10538&cfql=0)
        * "Dear students, To get you all well prepared for the upcoming practicals (and the Q10 exam) we have set up a series of questions that will be dealt with during the upcoming (interactive) lectures and group work sessions. Please prepare in **advance** by working your way through this **Biotechniques quiz** and collect your answers to the 14 questions. These will form the starting point for the interactive lecture. The questions build on the items dealt with in the E-learning module 1 (Rec-DNA and Recombinant Proteins), esp. in part I on Molecular Biological techniques. We encourage the use of this E-learning tool to discover or check your answers to the questions. Enjoy, Kind regards, Wiljan & Rick. **Starts Oct 16, 2019 10:31 | Due 07 November at 10:30**"

- [HC -- Q9 Lection series](https://brightspace.ru.nl/d2l/le/content/85951/Home)
    + LE - Lectures handouts
    + RC - IL response courses
    + **Exam prep moved to DOC**
- [PRJ -- Q9 Project Proposals](https://brightspace.ru.nl/d2l/le/content/85951/Home)
    + [-DEADlINE- Topics for Research Proposals](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399145/View)
        * "In this file you will find short descriptions of eight different Medical Biotechnology research topics, each linked to a dedicated tutor. After consulting the document, use the self-enrollment tool to subscribe to the team that will deal with your favorite topic. NB - it is our intention to have mixed teams, thus containing Radboudumc (~2) as well as NWI (~3) students per team, working on the subjects. We hope that this formation of multidisciplinary teams will occur naturally, otherwise we'll slightly 'edit' some teams. Make your choice before **Thursday 12 September, 2019**"
    + [Guidelines for writing a Project Proposal](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399153/View)
    + [Project Proposal FORM](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399154/View)
    + [User Instructions for Proposal FORM](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399150/View)
    + [Avoiding Plagiarism](https://xot.ru.nl/play.php?template_id=203)
        * "Plagiarism is a relevant item, also for students. The Library of Science offers an interactive e-learning module about plagiarism, in which information is alternated with quiz questions. Click on the link to start the module."
    + [-DEADLINE- Upload Minireview](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399119/View)
        * "Use this link to upload your document (.docx or .pdf) containing your team's Minireview. **Deadline: Sunday 29 September 2019, 23:59h.**"
    + [-DEADLINE- Upload (handout) Presentation Research Proposal](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399120/View)
        * "Use this link to upload your team's (powerpoint) presentation of the Research Proposal. These will also serve as handouts for your fellow students. **Deadline: Fri 01 November 2019, 12:30h.**"
    + [-DEADLINE- Upload Written Project Proposal](https://brightspace.ru.nl/d2l/le/content/85951/viewContent/399121/View)
        * "Use this link to upload the written version of your team's Research Proposal (as .docx or .pdf file). **Deadline: Fri 01 November 2019, 23:59h.**"
    + [-DEADLINE- Project 1 - Reports & Presentations](https://brightspace.ru.nl/d2l/lms/dropbox/user/folder_submit_files.d2l?ou=85951&db=21069)
        * "Project 1 - Reports & Presentations - Due 23:59. Use this link to hand in your written report (or your presentation handout) on Project 1 - Biologicals. **Deadline: 22 November 2019, 12:30h (presentations) and 23:59h (written reports)**."
    + [-DEADLINE- Project 2 - Reports & Presentations](https://brightspace.ru.nl/d2l/lms/dropbox/user/folder_submit_files.d2l?db=21070&grpid=0&isprv=&bp=0&ou=85951)
        * Use this link to hand in your written report (or your presentation handout) on Project 2 – Gene Therapy. **Deadline: Friday 13 December 2019, 13:30h (presentations) and 23:59h (written reports)**.
    + [-DEADLINE- Project 3 - Reports & Presentations](https://brightspace.ru.nl/d2l/lms/dropbox/user/folder_submit_files.d2l?db=21071&grpid=0&isprv=&bp=0&ou=85951)
        * "Use this link to hand in your written report (or your presentation handout) on Project 3 – Tissue Engineering. **Deadline: Friday 10 January 2020, 10:30h (presentations) and 23:59h (written reports).**"
    + [-DEADLINE- Project 4 - Reports & Presentations](https://brightspace.ru.nl/d2l/lms/dropbox/user/folder_submit_files.d2l?db=21072&grpid=0&isprv=&bp=0&ou=85951)
        * "Use this link to hand in your written report (or your presentation handout) on Project 4 – Precision Medicine Diagnostics. **Deadline: 24 January 2020, 13:00h (presentations) and 23:59h (written reports).**"

----

### Glossary ###

|  abbrev  |             def             |
|----------|-----------------------------|
| cDNA     | complementary DNA           |
| IF       | intermediate filament       |
| tracrRNA | trans-activating CRISPR RNA |
|          |                             |

- - - -

### EL01 -- Test yourself: Cell Biological & Biochemical Principles and Techniques ###

#### A -- Cell Biology Dogmas ####

- The 'DNA information flow' is not just unidirectional:
    + `DNA->RNA->Protein`
- Instead, RNA to DNA is also possible (by 'reverse transcriptase' enzyme used in cDNA synthesis):
    + `DNA<->RNA->Protein`
- [DNA replication -- Prokaryotes vs Eukaryotes](https://player.vimeo.com/video/353984354)
- [Protein synthesis](https://player.vimeo.com/video/353984048)

- An IRES (internal ribosomal entry site) sequence is found in viral messengers and enables the use of multiple open reading frames within an mRNA for translation purposes.
- The 'Kozak sequence' defines the optimal sequence for translation initiation on mammalian mRNAs.
- In prokaryotic messengers the 'Shine-Dalgarno' sequence, just in front of the AUG start codon, attracts the ribosomes. In eukaryotic mRNAs this element is missing that is essential for the initiation of translation in prokaryotes.

- **Reading frames**
    + Comparison of genomic and cDNA (mRNA-derived) sequences has learned which parts of the genome end up in transcripts. Comparison of protein sequences and mRNA-derived sequences subsequently has learned which parts in transcripts contain the actual protein coding information, i.e. make up the Open Reading Frame in the messenger RNA.
    + There are three potential reading frames on each strand.
    + **Stop codons**
        * TAA
        * TGA
        * TAG
    + **Start codon**
        * ATG on DNA
        * AUG in RNA
    + **Protein folding**
        * Eventually also the processing of the protein itself (that is encoded by the mRNA) is of importance for proper biological functioning. Most notably post-translational modifications (**such as glycosylation and proteolytic cleavage) can differ considerably between** prokaryotes and eukaryotes. And even before that, the amino acid chains that are produced need to fold correctly.
        * [Interactive animations -- Protein folding](https://www.wiley.com/college/boyer/0470003790/animations/protein_folding/protein_folding.htm)
    + **Protein transport**
        * Proteins exert their actions on very specific sites in the cell. They reach this destination by virtue of elegant protein transport and routing mechanisms. Which routing pathway acts upon a given protein depends on the signals present in the protein (its 'zip code').

| **protein transport signal** |   **destination**   |
|------------------------------|---------------------|
| Signal Peptide               | ER lumen            |
| Amphiphatic helix            | Mitochondria        |
| SKL                          | peroxisomes         |
| KDEL                         | resident ER protein |
| NLS                          | nucleus             |

- **Endoplasmatic Reticulum** produces:
    + lipids
    + proteins
- **Golgi**
    + After being synthesized at the ER, proteins leave via transport vesicles that 'bud' from the ER and then travel to the Golgi apparatus. After having travelled through the cis- and medial Golgi stacks, the proteins then reach the trans-Golgi network (TGN) where they are sorted to travel into one of three directions.
        * Post-Golgi
            - condensation
            - proteolysis
        * Golgi
            - glycosylation
            - CHO hydrolysis
            - phosphorylation
            - sulfation
            - proteolysis
            - sorting of proteins
        * ER
            - translation
            - signal proteolysis
            - protein segregation
            - primary glycosylation
- **Mitochondrion functions**
    + apoptosis regulation
    + `[Ca2+]` homeostasis
    + `[ATP]` production

- **Cytoskeleton**
    + The cell contains a 'framework': the cytoskeleton. This image provides in three different ways an overview of the three distinct cytoskeletal structures:
        * microfilaments (actin)
        * intermediate filaments (IFs)
        * microtubules.
    + The bottom panels give a schematic representation, the middle panels display electron microscopic images of purified filaments (at the same magnification) and the upper ones show immunofluorescent images for each of the cytoskeletal polymer types in cultured cells. In this particular example, to visualize IF proteins, epithelial cells (keratinocytes) were stained with an antibody against keratin.
        * ![cytoskeleton](./src/cytoskeleton.png)

#### B -- Replication and repair ####

- **Origin of replication**
    + To replicate DNA, the double-stranded helix first is opened up. Subsequently, an RNA primer is made (blue part) that is then elongated by DNA Polymerase (the red arrow), resulting in leading strand synthesis. Consequently, the replication fork is moving onward leaving the complementary, unreplicated piece of DNA as template. DNA primase (to generate the RNA primer) and DNA polymerase will then, by means of discontinuous DNA synthesis, also build the complementary version of that template: the so-called lagging strand synthesis.
    + ![DNA replication](./src/DNA_replication.png)
    + ![DNA replication fork](./src/DNA_replication_fork.png)
        * There is an 8-bp palindromic sequence in this image:
            - `DNA:  5'- AGAGCTCT -3'`
            - `cDNA: 3'- TCTCGAGA -5'` (using Okazaki fragments)
                * Enzymes that can cut this 8-bp palindromic sequence are:
                    - AluI (AGCT)
                    - SacI and SstI (GAGCTC).
- **DNA synthesis**
    + ![DNA synthesis diagram](https://upload.wikimedia.org/wikipedia/commons/8/8f/DNA_replication_en.svg)
        * NB. The `leading strand` is synthesized `5'- --> -3'` continuously
        * NB. The `lagging strand` is synthesized `3'- --> -5'`, but in fragments called Okazaki fragments.
- **DNA sequencing**
    + ![DNA synthesis diagram simple](./src/DNA_synthesis_diagram.png)
    + Torsion in the replication fork: `topoisomerases` ensure that the DNA is not entangled due to torsion and rapid unwinding (3000 rpm = 50 turns/s)
    + [Telomere replication animation](https://player.vimeo.com/video/353985355)
        * NB. To complete replication of the DNA sequence at the end of a strand, telomerase uses its own built-in RNA template that is complementary to the telomeric repeat sequence. So the telomerase is in fact an RNP (ribonucleoprotein) particle.
        * Cells with a high division turnover (e.g. germ/stem cells) have (proportionally) longer telomeres (germ/stem have on avg: 300--500 -TTAGGG- repeats)

----

### LE00 -- Introduction ###

- Course overview (see: TOP)

----

### LE01 -- Vaccines ###

- [Koch's postulates (1884) -- Wiki](https://en.wikipedia.org/wiki/Koch%27s_postulates): 4 criteria designed to establish a causative relationship between a microbe and a disease.
>    + \1. The microorganism must be found in abundance in all organisms suffering from the disease, but should not be found in healthy organisms.
>    + \2. The microorganism must be isolated from a diseased organism and grown in pure culture.
>    + \3. The cultured microorganism should cause disease when introduced into a healthy organism.
>    + \4. The microorganism must be reisolated from the inoculated, diseased experimental host and identified as being identical to the original specific causative agent.

- **Vaccine requirements**
    + safe, long-term protectivity
    + nature of pathogen
        * extracellular: neutralizing antibodies
        * intracellular: CD8+ CTL response
    + Ab & T-cell epitopes should include protective ones
    + cost-effective
    + biologically stable
    + ease of administration
    + no adverse side effects

- **6 Vaccine strategies**

|              Type              |      Form of protection     |             example              |
|--------------------------------|-----------------------------|----------------------------------|
| 1. Subunit[^1] \(antigen)      | Ab response                 | Tetanus toxoid, diphteria toxoid |
| 2. Conjugate (peptide)         | Th-cell-dependent Ab        | Haemophilus influenzae infection |
| 3. DNA                         | Ab + cell-mediated response | Clin. trials ongoing             |
| 4. Live attenuated/killed bact | Ab                          | BCG, cholera                     |
| 5. Live attenuated viruses     | Ab + cell-mediated          | Polio, rabies                    |
| 6. Vector (vir,bact)           | Ab + cell-mediated          | Clin. trials of HIV/vaccinia/TBC |

[^1]: 'Subunit' denotes that just one of all proteins of the MO to be vaccinated against is used, not a fragment of a MO-protein.

- 4+5. Live attenuated vaccines

`[Live, attenuated cells or viruses] == virulence eliminated/reduced ==>  
[Alive MO, with same antigenicity] == administration ==> vaccine simulates
immunity, and (attenuated) pathogen multiplies`

- \6. Vector (recombinant vaccines)
    + Gen. mod. viral vectors that can be used as a vaccine.
    + [Types of recombinant vaccines -- Type # 3](http://www.biologydiscussion.com/biotechnology/vaccines/types-of-recombinant-vaccines-3-types/10080) 

![Recombinant vector vaccines production](http://cdn.biologydiscussion.com/wp-content/uploads/2015/09/clip_image0206.jpg)

- \1. Subunit (antigen)
    + Comprised of (recombinant) components (prot, peptides, DNAs) of the pathogenic MO.
        * adv:
            - purity in preparation
            - stable
            - safe use
        * disadv:
            - high cost
            - possible alteration in native conformation (??)

![Subunit vaccines](http://cdn.biologydiscussion.com/wp-content/uploads/2015/09/clip_image00419.jpg)

- \2. Conjugate (peptide)
    + req./limitations:
        * epitope should exist in live pathogen
        * peptide conformation should mimic the structure in the intact pathogen
        * single epitope may not be sufficiently immunogenic

- \3. DNA (RNA)

|                |                adv                |       disadv       |
|----------------|-----------------------------------|--------------------|
| construction   | max freedom (plasmids & antigens) |                    |
| production     | non-pathogenic                    |                    |
|                | inexpensive                       |                    |
|                | large-scale options               |                    |
| safety         | absence of pathogens              |                    |
|                | no infection _in vivo_            |                    |
|                | cell-mediated immmune respone     |                    |
| stability      | long term storage (T insensitive) |                    |
| immunogenicity |                                   | weakly immunogenic |
|                |                                   |                    |

![safety_vs_efficacy](./src/eff_vs_safety.png)

- safety
    + precautions (extensive, expensive)
        * insufficient attenuation / killing?
        * back-mutant / virulence regain?
- production
    + not all infectious agents can be cultured
    + animal viruses: low yield, expensive culturing
- storage
    + limited shelf-life
    + refridgeration requirement
- administration
    + injections or oral, once or repeated

- - - -

### LE02 -- Genetic modification: Transfection & Transduction ###

#### Genetic modification ####

- **Genetic Modification**
    + episomal addition ('viral')
        * Life-time: transient
    + insertion ('transgene')
        * Life-time: stable
    + deletion ('knock-out')
        * Life-time: stable
    + replacement ('knock-in')
        * Life-time: stable

- **Modification life-times** \([Transfection -- Wiki](https://en.wikipedia.org/wiki/Transfection#Stable_and_transient_transfection)\)
    + Transient
    + Conditional
    + Stable
        * random
            - insertion
            - mutation
        * targeted
            - homologous recombination
            - site-specific recombination

![expression construct](./src/expression_construct.png)

#### Methodology ####

- **Transfection Methods**
    + Physical
        * micro-injection (transgenic mice)
            - is relatively easy and effective, but not as precise
        * Electroporation (KO/knock-in mice)
            - Electrophoresis of negatively charged DNA
        * Particle bombardment (plants, genetic vaccination)
            - shooting genetic material _into_ the cell (helium-pressurized nowadays)
    + Chemical
        * Calcium phosphate precipitation (cell, lab) `=> see fig. below`
            - Calcium phosphate attracts immune cells and transfection occurs through some cells endocytosing the particles.
        * DEAE dextran uptake
        * Lipofection
            - very effective (up to 100%) method
            - required compounds are very expensive
    + Biological
        * Cell fusion (hybridomas)
            - Fusion of myeloma cells (cancerous, rapidly dividing) and spleen cells (Ab-producing cells)
            - after cell fusion you'll have to get rid of the non-fused myeloma and spleen cells:
                + spleen cells won't grow
                + the myeloma cells used are crippled genetically
            - Virus infection (gene therapy)
                + retrovirus/lentivirus?? search for non-coding sequences of DNA, resulting in limited DNA insertion damage.
                + adenovirus remain in the target's genetic code (for longer periods of time)
                + adenovirus-associatiod viruses (AAV)

![CaP3](./src/CaP3.png)  
_Fig. Calcium Phosphate precipitation_

![virus_infection](./src/virus_infection.png)  
_Fig. Virus infection_

#### Transgenesis ####

- **Generating transgenic organism**
    + Genome is modified by gen. engineering
    + modification is transmitted to offspring
    + 4 methods
        * \1. retroviral infection
            - insertion damage
            - limited insert size
            - results in mosaicism
        * \2. oocyte micro-injection
            - insertion damage
            - multiple copies ('concatemers')
                + ~40% of eggs wild evelop to term
                + ~10% of animals born are transgenic
        * \3. Embryonic stem (ES) cells
            - Electroporation of ES cells (single integration)
            - in vitro selection
                + random insertion
                + targeted integration
            - ![dsDNA break](./src/dsDNA_repair.png)
                + left: NHEJ (non-homologous end joining) (random recombination)
                + right: HR (homologous recombination) (_homologous recombination_)
            - ![homologous recombination](./src/homologous_recombination.png)
        * \4. Somatic Cell Nuclear Transfer (S.C.N.T.)
            - only possible if both nuclei are at/in the same developmental stage
            - electrofusion
            - injection
                + reproductive cloning?
                + therapeutiuc cloning?
            - application: mitochondrial disease prevention (IVF with non-diseased mitDNA)

![mito_disease_prev](./src/mito_disease_prev.png)

- **GMO applications**
    + more complex research
    + disease model creation
    + stem cell research/therapy
    + bioindustry (improve livestock)
    + biomedical industry
        * pharmaca
        * designer mice: human disease models for testing
        * Induced pluripotent stem cells (`see Figs. below`)
            + modifying regular cells into pluripotent ones (very ineffective, ~0.01%)
        * Adult stem cells (`see Figs. below`)
        * cancer research: xenografts, organoids (`see Figs. below`)

- **Induced Pluripotent Stem Cells** (iPS)

![iPS](./src/iPS.png)

![iPS_potential](./src/iPS_potential.png)

![adult_stem_cells](./src/adult_stem_cells.png)

![cancer_research](./src/cancer_research.png)

- - - -

### CC1 -- Rec-DNA and Recombinant Proteins ###

- **Immunoblotting**
    + In analogy with the Southern and NortherN blots - that are used for the detection of DNA and RNA, respectively - a technique has been developed to transfer proteins - following their separation on a gel (e.g. by SDS-PAGE) - onto a solid support (mostly nitrocellulose paper). The separated and blotted proteins can then be sujected to immunodetection using antibodies: immunoblotting.

- **Recombinant vaccines**
    + Some examples of the application of recombinant proteins in the field of medical biotechnology. The animation on the right deals with the construction of recombinant vaccines. By forcing harmless host cells, via genetic modification, to produce a recombinant protein that is specific for a disease-causing agent a low-risk vaccine can be generated.
- [McGrawHill -- Constructing Vaccines video](https://player.vimeo.com/video/353984503)
- [Tetracycline resistance -- Wiki](https://en.wikipedia.org/wiki/Tetracycline#Mechanisms_of_resistance)

- **Restriction endonucleases**
    + Restriction endonucleases recognize and cut specific sequences of dsDNA. Additionally, many of them do not digest sstraight across the DNA, but instead leave ends where one DNA strand hangs out further than the other. These overhanging ends are called 'sticky ends' or 'compatible ends', because, when DNAs are being cut, the overhangs can base-pair with each other and facilitate ligation.

| Restriction Enzyme | Recognition seq |  Ends generated  |
|--------------------|-----------------|------------------|
| AluI               | --AGCT--        | --AG        CT-- |
|                    | --TCGA--        | --TC        GA-- |
| BamHI              | --CGATCC--      | --G      AATTC-- |
|                    | --CCTAGG--      | --CTAA       G-- |
| SacI               | --GAGCTC--      | --GAGCT      C-- |
|                    | --CTCGAG--      | --C      TCGAG-- |

- - - -

### LE03 -- Advanced clining, engineering and genetic modification ###

|  abbr  |                        def                        |
|--------|---------------------------------------------------|
| hESC   | human embryonic stem cells                        |
| ss-    | single strand                                     |
| ds-    | double strand`                                    |
| operon | a functioning unit of DNA containing a cluster of |
|        | genes under the control of a single promoter      |
    
#### Genetic recombination ####

- **Why is there recombination in nature?**
    + source of gen. diversity, providing raw material for evolution
    + it serves to physically link pairs of homologues in Meiosis I
    + In some organisms, it is used to maintain telomeres
    + **It is required to maintain genome integrity in the face of DNA damage such as ss-breaks and ds-breaks**

- **Genetic recombination mechanisms in nature**
    + site specific recombination
    + transposition
    + illegitimate recombination (non-homologous (=random) end joining; NHEJ ; insertion in non-coding DNA)
        * happens in viral infection; the virus will (depending) be able to have it's sequence translated by activating/promoting ('turning on') their newly inserted sequence.
    + homologous recombination (insertion in coding DNA)
    + destruction of invading DNA (CRISPR/Cas9)
    + for recombining, CRISPR/Cas9 is used to target chromosomal DNA strand breaks to specific DNA sequences. `Dead` (no break), `single` and `double` strand breaks are possible

- **Why engineer genomes?**
    + Genotype-phenotype relation is a cornerstone of molecular biology/functional genomics research!
    + Germline transmissino of mutations can be introduced into the mouse genome. **This permits generation of mouse models that recapitulate human conditions accurately**, for example to test therapies.

#### Genetic recombination 2 ####

`plasmid point mutagenesis: PCR the plasmid with mutant primers`

- **plasmids**
    + Are found in archeae, prokaryotes and eukaryotes. We focus on bacterial plasmids.
    + Plasmids are extra-chromosomal dsDNA elements, usually circular.
    + Plasmids are transmitted _'vertically'_ to the two daughter cells upon bacterial cell division.
    + Plasmids can be transmitted _'horizontally'_ from one bacterium to another via a process termed _'bacterial conjugation'_
    + pBC (Bluescript) is an artificial plasmid for biotechnology. _It is not conjugative! (??)_

- **plasmid point mutation / basic principles**
    + \1. PCR your plasmid with two complementary primers that harbour the desired point mutation (A,C,G,T ; shown as \'`*`\')
    + Cut the start vector to pieces with DpnI (only cuts GAmeTC) into ~15 pieces, not the PCR product as that has non N6-methyl adenine GATC sequences.
    + transform into E. coli


![plasmid_DpnI](./src/plasmid_DpnI.png)

#### Genetic recombination 3 ####

`site specific recombinases: Gateway cloning in bacteria`

|  abbr  |      def       |
|--------|----------------|
| att`X` | attachment `X` |
| B      | bacterium      |
| P      | phage          |
| φ      | phage          |
| L/R    | left/right     |

![gateway_system](./src/gateway_system.png)  
_Fig. The gateway system: massive parallel cloning without restrictino enzymes, but using Lambda phage enzymes_

![phage_integration](./src/phage_integration.png)  
_Fig. phage integration and prophage excision have different effectors: directional site-specific DNA recombination_

- The phage carries attP
- E. coli chromosome carries attB
- prophage  (integrated carries attL and attR)

- An invading phage can be integrated into the E. coli chromosome at the at the `AttB` site to become a 'prophage'. There it remains in a dormant (so-called _latent/lysogenic state_). When the prophage is active, it is said to be 'lytic', which is the infectious state.
- The transition from lysogenic to lytic cycle is caused by stress (time to ~~flee the~~ abondon ship), and involves proteolytic inactivatino of the trancription repressor cI. This results in transcription from P(\_R) and P(\_L). Then the lytic phage genes are expressed, this includes XIS and INT recombinases, which excise the phage genome to allow its rapid replication via a rolling circle mechanism and phage particle production, ultimately leading to cell lysis to release infectious phages.

- Phage Y can recognize stress, exploit it's host bacterium to synthesize great numbers of phages and leave the bacterium
    + phage Y is generally altriuistic, but will leave 'a sinking ship' (`see before: """- The transition from (...)"""`)
- Phage integration is reversible; enzymatic process involved:
    + integration: `attB + attP => attL + attR` --BP-- (INT + IHF)
    + excision: `attL + attR => attB + attP` --LR-- (INT + IHF + XIS)

![phage_int_exc](./src/phage_int_exc.png)

- ccdB is a toxic protein with a t(\_1/2) of 2h. In vivo it is counteracted by ccdA with a t(\_1/2) of 1h. They form the plasmid addiction system of natural plasmid `(???)`. ccdB inhibits gyrase.
- An E. coli strain with a GyrA462 mutation is resistant to ccdB and is used to propagate the 'empty' destination vector.
- Entry clone is selected with Kan(^R), destination factor with Amp(^R).

#### Genetic recombination 4 ####

`site specific recombination applications in eukaryotic cells`

![plasmid_flipcut](./src/plasmid_flipcut.png)

- INT from phage Y contains a catalytic triad, similar to topoisomerasses (which control DNA-winding), which is also present in [CRE](https://en.wikipedia.org/wiki/Cre_recombinase) (from bacteriophage P1) and [FLP](https://en.wikipedia.org/wiki/Flippase) (from yeast plasmid).

#### The Rosa26 locus ####

`position effect variation (PEV, the influence of the chromosome insertion site on transgene expression level) is a real pain when making transgenic mice or cell lines. Rosa26 is a mouse locus that is not subject to PEV.`

#### Genetic recombination 5 ####

`Cell lineage tracing: demonstration of stem cell identity`

![lineage_tracing](./src/lineage_tracing.png)

- Transient activation of CRE permanently activates the LacZ gene at ROSA26 by deleting the STOP cassette in front of the LacZ open reading farme, which permanently marks all the descendants of that cell.


#### Summary ####

- Plasmids are 'minichromosomes' that can be used to propagate DNA in bacterial (E. coli) populations.
- PCR can be used to change single nucleotides into plasmids. Current protocols involve inactivation of the original plasmid prior to introduction of mutant plasmid into bacteria.
- Site-specific DNA recombinases INT, CRE and FLP use 34bp sequences to catalyse DNA recombination (cut **and** paste).
- INT (+ XIS + IHF) are used for _massive parallel_ gateway _cloning_.
- CRE and FLP can be rendered ligand-dependent. This is useful for many _in vivo_ experiments, including lineage tracing.
- Recombining uses homologous DNA recombination for precise, specific and faithful chromosome engineering
- CRISPR RNA guides the Cas9 endonuclease to induce DNA breaks to provoke repair reactions that can be exploited for recombining.

- - - -

### LE04 -- stem cells ###

| abbr |           def            |
|------|--------------------------|
| ES   | embryonic stem cells     |
| EG   | embryonic germinal cells |

- **Stem cell and gene therapy research are closely related biomedical fields**
    + An ideal scenario entails that a patient with a genetic defect would be cured by modification of his/her own stem cells.
    + Step 1: Isolate (stem) cells from the pt.
    + Step 2: Modify the (stem) cells
    + Step 3: Implant the modified (stem) cells
    + i.e.: like a bone marrow transplantation but with an added gene therapy intervention (SCID)
    + ...: or skin transplantation, but with a gene therapy intervention (Epidermolysis bullosa)

#### Empirical classification of stem cells ####

- Totipotent stem cells
    + can give rise to _all_ human cell types (= the zygote, blastomeres in very early embryos)
- Pluripotent stem cells
    + limited differentiation potential; often _excluding extra embryonic tissues_.
    + e.g.: (i) about 100--1000 embryonic stem cells are found in the blastocyst 
    + ...: (ii) Embryonic germinal cells
- Multipotent stem cells
    + = adult stem cells
    + restricted to one 'lineage'
        * e.g. mesenchymal, haematopoietic, neural

" (...) a very strong Swiss website to brush up \[on] your embryology knowledge: [link](http://www.embryology.ch/anglais/hdisqueembry/triderm07.html)"

- **Somites**
    + human somites (n=42--44, becomes 37) form consecutively between embryonic day 16 -- day 26 at a rate of 3--4/d.
    + chicken embryos have 50 somites, mice have 65, snakes have 500.

#### Embryonic development layers in adults ####

- dermatome: skin area innervated by branches of _one_ spinal nerve
- sclerotome: cartilage and bone
- myotome: portion of somite mesoderm which form the skeletal muscles of the neck, trunk, limbs.

![dermatome](./src/dermatome.png)

#### Complex niches of stem cells ####

- total different cell types in mammals: >200
- some last a lifetime, some 120d (erythrocytes = RBCs), some ~5d.
- estimated RBCs generated per second from a pool of HSCs: 2.5*10(^3).

- HSC are made in the aorta-gonad-mesonephros region of the early embryo, and are derived from endothelial-like cells.
- Later HSCs migrate to the embryonic liver, then to the foetal speel, \[in order to] finally migrate to the marrow around birth.
- These migrations are controlled by cytokines such as SDF-1 and steel factor (SLF). HSCs from adults are sensitive to SDF-1.

-- `??????` --

#### Asymmetrical division of cell types ####

![SC_multiply](./src/SC_multiply.png)

- this makes multiplication of SCs in vitro difficult, because 1 of the 2 daughter cells will differentiate (regardless of ssubsequent division (or not)).
- there is  a hunt for growth factors enabling amplification of stem cells.

#### (Experimental) Isolation of SCs ####

- origin in 2006 study; 2 extremely well-cited papers
    + Takeshi et al. 2006 \}  
    + Takeshi et al. 2007 \}\} 2012 Nobel prize  
        * transcription factors:
            - OCT4
            - KLF4
            - SOX2
            - cMYC

- Retrodifferentiation (?)
    + this can be accelerated with several small molecules (?) 
        * histone deacetylase inhibitors
        * G9a histone H3K9 methyl transferase inhibitor
        * Kenpaullone (also GSK3 inhibitor)
        * TGF-beta RI Kinase Inhibitor II 616452 helsp to replace SOX2
        * current cocktail: `VPA + CHIR99021 + 616452`

#### 2i: medium makes mouse ES cells that are much more homogenous _`and that grow without added bovine serum`_ ####


#### 2i: two kinase inhibitors that strongly favor ES cell growth => in vitro culture in the absence of bovine serum is possible with 2i  ####

![kinase-I](./src/kinase-I.png)

- ERK inhibition through inhibition of ERK kinase
- Concomitant inhibition of GSK3 promotes ES cell viability
- Mouse ES cells: BMP and LIF promotes viability of mouse ES cells by inhibiting commitment to differentiation.

#### Asymmetrical cell division realizes the regenerative power ####

- SC manipulation may help cure many diseases: "-potencies
- In-vivo or ex-vivo gene therapy are sister fields to stem cell research. They may permit tailored genetic corrections. Organoid cultures are the latest innovation.
- What are we looking at? => embryology, germ layers, cell fate commitment and somitogenesis
- ESCs can proliferate and retain their identity. Many cell types can be converted into ES cells 'symply' by expressing the **4 transcription factors: OCT4,KLF4,SOX2,cMYC**.
- Cell fate commitment often involves WNT and NOTCH, which, together, function as an electronic transistor (amplify, noise reduction, permit choices).
- Small compounds can replace genetic tools. But you need to know what to target (...)
- organoids-(5 * (organoids))  (???)

- - - -

### LE05 -- Genetic therapy - the biotechnical toolbox ###

#### Glossary ####

| abbr |         def         |
|------|---------------------|
| BBB  | blood-brain barrier |

#### definitions ####

- symptomatic therapy = any medical therapy for a disease that only affects its _symptoms_, _not_ its cause.
- molecular therapy = therapy with compounds that are _rationally targeted_ against certain specific molecular structures (usually associated with the _cause of disease_).
- gene therapy = introduction and expression of novel _genetic information_, or _editing of the genome_ in cells for therapeutic purposes. _Usually permanent_. Can be _ex vivo_ or _in vivo_.`
- genetic therapy = delivery of nucleotide sequences or proteins or small molecules to correct the function of a defective gene or compensate for its functional loss or gain. _Usually not permanent_.

![therapeutic_strategy_design](./src/therapeutic_strat_design.png)

#### Context and perspectives in genetic therapy ####

- **Science**
    + Biotechnical developments can be limiting
    + Relevant cell and animal models required for testing
    + Biomarkers and clinical outcome measures needed for trials
    + Drug specificity (on- vs off-target) and adverse effects
    + Delivery (local, systemic, BBB)
- **Ethics**
    + Care (= treat) versus cure (= healthy)
    + Pt. advocacy groupds are influencers and have power
    + Somatic vs. germ line therapy (next generation)
    + Sign. clin. benefit over existing therapies
    + Cost effectiveness: QALY
        * 80 000 € pp/y (per person per year)
- **Economy, politics, regulation**
    + Costs of development: >M€ 100--500/drug
    + Participation of industry is required (?)
    + Intellectual property (IP)/patent (20y)
    + Regulatory agencies (FDA, EMA)
    + Time: development takes at least 10--15y

![gen_tx_timeline](./src/gen_tx_timeline.png)

#### Biotechnological toolbox and corresponding strategies ####

- **DNA**
    + DNA or chimaeric RNA--DNA oligunucleotides: targeted gene editing/repair
    + ![dna1](./src/biotechbox-dna1.png)
    + anti-gene oligonucleotides: transcription inhibition
    + ![dna2](./src/biotechbox-dna2.png)
    + small synthetic drugs: (DNA methylation/histone modification)
    + ![dna3](./src/biotechbox-dna3.png)
    + CRISPR/Cas, zinc finger nucleases, TALENs, meganucleases: genetic engineering
    + ![dna4](./src/biotechbox-dna4.png)
    + ![dna5](./src/biotechbox-dna5.png)
- **RNA**
    + ribozyme: RNA degradation
    + ![rna1](./src/biotechbox-rna1.png)
    + antisense oligonucleotides: RNA degradation/translation inhib./splicing modulation
    + ![rna2](./src/biotechbox-rna2.png)
    + siRNA (small interfering RNA): RNA degradation
    + ![rna3](./src/biotechbox-rna3.png)
- **Protein**
    + small synthetic molecules: protein inhibition/induction/activation
    + aptamers/RNA decoys: protein binding/inhibition
    + ![prot1](./src/biotechbox-prot1.png)
    + recombinant protein: enzyme replacement (_M. Pompe_), antibodies
- **Cell/tissue metabolites**
    + vitamins/hormones: replacement therapy (_Congenital hypo\[Thyr]_)
    + special diet: dietary restriction (_Phenylketonuria_)
    + iPSC: stem cell therapy/cell replacement therapy
    + tissue transplantation (_Adult-onset polycystic kidney disease_)
    + removal of diseased tissue (_Familial adenomatous polyposis_)

- - - -

### LE06 -- Genetic therapy - disease examples and treatment strategies ###

#### Examples of genetic therapy ####

- **DMD** (= M. Duchenne)
    + antisense oligonucleotides: exon skipping (Eteplirsen/Exondys 51)
    + X-linked recessive
    + progressive muscular dystrophy
    + Symptoms:
        * severe, prog muscular dystrophy
        * affects mostly boys (X-linked; inherited/30% de novo)
        * prvl=1/3500 male births
        * \>12y: wheelchair bound
        * \>20y: assisted ventilation
    + Molecular pathogenesis (1)
        * mutation in dystrophin gene (2.2Mbp, largest protein-coding gene in humans)
        * 427 kDa dystrophin protein connects cell membrane and cytoskeleton (in muscles)
        * muscle weakness and damage => connective and fatty tissue infiltration
        * ![dystrophin](./src/dystrophin.png)
        * ![dystrophin_biopsy](./src/dystrophin_biopsy.png)
    + Molecular pathogenesis (2)
        * `_DNA` ↓ `point mutation/deletion/duplication in dystrophin gene`
        * `prot` ↓ `no or dysfunctional dystrophin protein`
        * `cell` ↓ `improper attachment of cytoskeleton to cell membrane`
        * `tiss` ↓ `rupture and dagae of muscle fibres during contraction`
        * `''''..`  `muscular dystrophy, connective tissue replacement`
- **SMA** (= spinal muscular atrophy)
    + antisense oligonucletoides: exon inclusion (Nusinersen/Spinraza)
    + autosomal recessive
    + neurogen. disorder
    + Sx:
        * overal muscle weakness
        * disequilibrium (difficulty sitting/standing/walking)
        * SMA type I--V (high variable, very severe -- mild)
        * onset >30yo (congenital)
        * reduced life expectancy in severe cases
        * inheritable (autosom. rec.)
        * prvl-carrier = 1/50 (in _NL._)
    + Molecular pathogenesis    
        * mutation in/loss of both SMN1 (=Survival of MotorNeuron 1 protein) genes 
        * ubiquitous expression (high in spinal cord)
        * role in mRNA processing, in particular in dendrites and axons
        * essential for survival of motor neurons
        * death of motor neurons => muscle wasting (atrophy)
    + Molecular pathogenesis (2)
        * `_DNA` ↓ `two non-functional SMN1 genes`
        * `prot` ↓ `no, insufficient or nonfunctional SMN1 protein`
        * `cell` ↓ `insufficient outgrowth and maintenance of axons`
        * `tiss` ↓ `death of motor neurons, muscle atrophy, mobility impairment`
- **CF** (= cystic fibrosis)
    + Ataluren/Translarna (PTC124): read through of premature stop codon
    + small molecules: potentiators and correctors (protein folding chaperones)
    + autosomal recessive
    + multisystemic lung disorder
    + Sx:
        * inc = 1/2500 live births
        * thick, sticky mucus production
        * incessant couging
        * respiratory infections
        * salty tasting skin (sweat)
        * poor growth and weight gain
        * GI-problems (pain, constipation)
        * fibrotic pancreas
        * infertility (absence vas deferens)
    + Molecular pathogenesis
        * mutation in the CFTR (cystic fibrosis transmembane conductance regulator)
        * ABC-transporter-type ion channel on cell surface
        * blockage of ion transport (Cl-) in epithelial cells
    + Mutations vs CFTR dysfunction
        * `_DNA` ↓ `nonsense/missense/deletion mutations in CFTR gene`
        * `prot` ↓ `no or dysfunctional CFTR protein`
        * `cell` ↓ `improper ion transport, altered secretions`
        * `tiss` ↓ `blocked ducts => impaired mucal defense, infection, inflammation`
    + Tx:

![cf_class](./src/cf_classification.png)
![cf_tx_1](./src/cf_tx_1.png)
![cf_tx_2](./src/cf_tx_2.png)
![cf_tx_3](./src/cf_tx_3.png)

- **HGPS** (Hutchinson Gilford Progeria Syndrome) 
    + autosomal dominant
    + Disease characteristics:
        * prvl = 1/4 000 000 newborns
        * \>200 reported cases
        * premature aging
        * death at !13.5yo
        * laminopathy
    + Molecular pathogenesis
        * mutation in LMNA gene
        * incomplete lamin A protein processing

![hgps_1](./src/hgsp_1.png)
![hgps_2](./src/hgsp_2.png)
![hgps_3](./src/hgsp_3.png)

- - - -

### RC05-06 -- genetic therapy and its tool box ###

**ZSO questions are similar to exam questions!**


##### 1. What is genetic therapy? #####

-- ommitted by teach --

> **Course Materials** 
> 
> - Assigned papers:
>      + Daya & Berns (2008) Gene therapy using adeno-associated virus vectors. Clin Microbiol Rev. 21(4): 583--593. PMID: 18854481.
>      + Kay (2011) State-of-the-art gene-based therapies: the road ahead. Nat Rev Genet. 12(5): 316--328. PMID: 21468099.
> 
> - Background reading: 
>      + Debyser (2003) A short course on virology / vectorology / gene therapy. Curr Gene Ther. 3(6): 495--499. PMID: 14683447.
> 
> - Website:  
> To prepare for this lecture (and later on for the exam) you may have a look at: <http://learn.genetics.utah.edu/content/genetherapy>

##### 2. Biotechnological tools for therapeutic strategies #####

> Genetic therapies can be developed thanks to the possibilities offered by biotechnology. Use information from the lectures, the paper by Lee et al (World J. Gastroenterol. (2013) 19:8949-8962) and information on the web to answer the following question. Genetic therapies are based on a wide range of very diverse biotechnological molecules, materials, methods and techniques. Describe for each of the 'tools' a-k below... 
 
- \2.1. the nature of the molecule (what type of molecule is it and what is it made of?)
- \2.2. its intrinsic activity (what does it do?),
- \2.3. for what purpose it can be used (in a therapeutic setting).
 
- a. ribozyme
    + RNA
    + endonuclease (RNA cleavage)
    + control expression    
- b. aptamer -- RNA or DNA
    + RNA or DNA
    + high affinity binding targetg molecules
    + inhibition or activation of target molecule or block binding to another compound.
- c. Zinc Finger Nuclease (ZFN)
    + protein
    + endonuclease (DNA, ds break)
    + gene editing/genetic engineering
- d. (ant)agonist 
    + 'small' compound
    + high affinity binding to target
    + inhibition/activation/conform.change of target
- e. antisense oligonucleotide 
    + modified RNA/DNA
    + binds RNA or DNA
    + RNA dxegradation, splicing modulation, translation inhibition, etc.
- f. sense oligonucleotide
    + modified RNA/DNA
    + binds to template strand (= same sequence as the DNA it binds)
    + transcription inhibition (only at high binding strength)
- g. TALEN (Transcription Activator- Like Effector Nuclease)
    + protein
    + endonuclease (DNA, ds break)
    + gene editing genetic engineering
- h. siRNA 
    + dsRNA
    + binds RNA
    + RNA degradation or translation inhibition
- i. recombinant protein
    + protein
    + enzymatic, signaling or structural function
    + therapeutic protein replacement 
- j. antibody
    + protein
    + binds antigen (anything)
    + inhibiition, absorption, activation
- k. CRISPR/Cas9
    + protein + RNA complex
    + endonuclease (DNA, ds break)
    + gene editing/genetic engineering
 
 
##### 3. Genetic therapy for Duchenne muscular dystrophy (DMD) #####
  
- 3.1. The variety of mutations in the dystrophin gene that lead to DMD is enormous. Yet, quite often one particular exon skipping strategy may be useful to treat multiple mutations. As an example, it is postulated that 6% of DMD patients, based on their disease mutation, qualify for a therapy aimed at skipping of exon 44.
    + (i)  Predict for each of the following mutations whether it will lead to DMD, BMD, or neither. 
    + (ii) Determine whether skipping of exon 44 may have benefit for a patient with that particular 
    + del exon 43
    + nonsense mutation (premature stop codon) exson 44
    + del exon 45
    + del exon 47
    + del exon 38--43
    + del exon 42--43
    + del exon 45--52
    + del exon 46--54
- \3.2 Another pt with DMD carries a nonsense mutation in exon 51
    + (i) Design a therapy based on exon skipping.
    + (ii) Consider a therapeutic strategy that is not based on the use of oligonucleotides.
- \3.3. It is clear that an exon skipping therapy for DMD is an excellent example of precision medicine (personalized medicine). Explain on the basis of the molecular pathogenesis of DMD whether all DMD patients could be helped by an exon skipping therapy.
    + Not all DMD-pts can be helped; e.g. those missing beginning or end of the gene (obv.)
 
##### 4. Genetic therapy for spinal muscular atrophy (SMA) #####

> The pathology of SMA was discussed during the lecture. Use this information to answer the questions below. First, watch the animation <www.dnalc.org/view/16941-2D-Animation-of- Alternative%20RNA%20Splicing.html>
 
- \4.1. The animation shows that apart from the SMN1 gene there is the SMN2 gene. Both genes are nearly identical. Explain why in patients with SMA the SMN2 gene will not be able to take over the function of the mutated SMN1 gene.
- \4.2. In the animation some sort of lay language is being used: "The mutation in SMN2 blocks a helper protein involved in splicing (an 'enhancer') so that exon 7 is not included in the mRNA."You certainly can do better! Use proper academic molecular and mechanistic terms to explain what is meant here.
> The therapeutic strategy for a genetic therapy for SMA is discussed in a second animation: <www.dnalc.org/view/16950-2D-Animation-of-Antisense%20Oligonucleotide%20Therapy-for-SMA.html>

Defect in SMA is in proteins SMN1 & SMN2; SMA is the result of (...faulty) exon skipping

- Regulation of splicing by splicing enhancers and silencers:
    + SE (splicing enhancers) ....
    + SS (splicing silencers) ....

- \4.3. 
    + (i) Describe the principle of the strategy in general terms: what is missing in a patient with SMA and how is it restored by therapy?
        * **common mistake; pre-mRNA molecule: oligonucleotides cannot be used on mRNA (because it has already been spliced)**
    + (ii) Now explain the therapeutic strategy in detail and pay attention to the following questions:  
    + a. What is the molecular target? 
    + b. The therapy interferes at which level in the disease mechanism (DNA -> RNA -> protein --> cell/tissue)?
    + c. Which biotechnological tool is being used? 
    + d. How is the tool produced or obtained? 
    + e. What is the effect of the target-tool interaction? What is the mechanism?  
    + f. What is the target sequence for the tool? 
- \4.4. Can this therapeutic strategy be used for all types of mutations in the SMN1 gene? 
- \4.5. Nusinersen (Spinraza™) is the antisense oligonucleotide drug that is on the market for SMA. Give one logical reason why this drug is probably so effective, in contrast to for example Drisapersen or Etiplersen for DMD. Read for example _Corey, Nature Neuroscience, 20:497 (2017)_ and watch one of the many videos on the internet on this topic, e.g. <https://www.youtube.com/watch?v=YDFIcgjB1j4>, also featuring inventor Adrian Krainer in the end.
- \4.6. Consider one or more alternative therapeutic strategies for SMA, and describe the target and tool. Try first to think out of the box and then consider whether your idea is actually feasible. You can uyse any tool that was discussed in this Medical Biotechnology course.

- - - -


- - - -

### LE07 -- Molecular tumor genetics ###

![targeted_tx](./src/targeted_tx.png)
![egfr](./src/egfr.png)

![nsclc_pathway](./src/nsclc_pathway.png)

|      | nsclca |          sclca           |                   |
|------|--------|--------------------------|-------------------|
| Epix | ~85%   | ~15%                     | % of all patients |
|------|--------|--------------------------|-------------------|
| prop |        | - shorter doubling time  |                   |
|      |        | - higher growth fraction |                   |
|      |        | - earlier meta's         |                   |
|------|--------|--------------------------|-------------------|
|      |        | - initially very         |                   |
|      |        | susceptible to chemoTx;  |                   |
|      |        | majority of pt. evolve   |                   |
|      |        | resistence mn-1y into Tx |                   |

- Resistance to RTK inhibitors an new inhibitors
    + acquired EGFR p.T790M mutation, most important resistance mutation to first/second generation RTK inhibitors (erlotinib, gefinitib)
    + osimertinib
    + acquired EGFR L718V mutation, resistance mutation to osimertinib Tx

- Tissue processing
    + ![fix_vs_ffpe](./src/fixation_vs_ffpe.png)
    + great quality for pathology ; great preservation for microscopy.
    + not for molec.sci: RNA and DNA long chains stick together and are broken up in small genetic/genomic(?) fragments

#### Sequence analysis ####

![seq_analysis_cap](./src/seq_analysis_cap.png)

#### Sanger sequencing (= DiDeoxy sequencing) ####

- In this method,nucleotides (A,C,T,G) and terminating nucleotides are incorporated into the synthesized DNA chain during sequence response.
- As a result, DNA chains of different lengths are synthesized in sequence response of a DNA region.
- The DNA chains of different lengths are separated by electrophoresis and analyzed after completion of the sequence reaction.
- The signal detected results in a pattern of peaks.

![sanger1](./src/sanger1.png

#### NGS (= massive parallel sequencing) ####

- NGS, in which DNA fragments of different genes and gene regions are generated at the same time. 
- The techonology is based on the 'sequencing by synthesis' principle. 
- In the synthesized strand, nucleotides (A,C,T,G) are incorporated during the sequence reaction. Each nucleotide that is incorporated gives a unique signal detected during the synthessi reaction, hence the term "_sequencing by synthesis_". 
- Because the individually synthesized DNA chians, reads, are detected, this technique is very sensitive



- \1. library prep (cluster generation?)
- \2. amplification
- \3. sequencing
- \4. analysis

![ample_seq_anal](./src/ampl_seq_anal.png)
![seq_methods](./src/seq_methods.png)

![how1](./src/how1.png)
![how2](./src/how2.png)

![sanger_seq](./src/sanger_seq.png)

![mutation_detection_NGS](./src/mutation_detect_NGS.png)


#### NGS (Illumina) ####

- lib construct
- cluster generation
    + add and hybridize the library to the flow cell
    + bridge amplification
    + generating clusters from individual molecules
- seq
- uencing
    + add all 4 fluorescently tagged nucleotides; 
    + single base is incorpated at a time; reversible terminator is on every nucleotide to prevent multiple additions in one round 
    + remove terminator and repeat
    + resulting image is in 4 colors

![ngs_illumina1](./src/ngs_illumina1.png)

`NGS measure light/specific fluorescent tags`

#### IonTorrent (life technologies) ####

- _sequencing signal is a proton \[H+]_; proton is released with every sequenced nucleotide:

![NGS_iontorrent_graph](/home/intronaut/syy/nextcloud/dox/med/8/MIN-RES/src/NGS_iontorrent_graph.png)

- nucleotides enter one type (types are recorded by computer), every time. They are washed out and the next nucleotide is introduced
- repeat

![iontorrent_workup1](./src/iontorrent_workup1.png)
![iontorrent_workup2](./src/iontorrent_workup2.png)
![iontorrent_workup3](./src/iontorrent_workup3.png)
![iontorrent_workup4](./src/iontorrent_workup4.png)

`IonTorrent measures ions/protons \[H+]`

- - - -

### LE08 -- identification of chromosomal abberations in tumours ###

![chrom_gen_aberr](./src/chrom_gen_abber.png)

![ERBB2R_trastuzamab](./src/ERBB2R_trastuzamab)


- - - -

### RC07-08 -- Molecular tumor genetics ###

> **Aim**  
> To review obtained knowledge on molecular tumor genetics and the identification of chromosomal aberrations, and iron out uncertainties, unclarities and misconceptions.
>
> **Instructions**  
> The field of molecular tumor pathology is continuously evolving, which involves novel technologies and further improvements and refinements of the technologies that are essential to detect the molecular aberrations for diagnosis, prognosis and therapy response prediction in patients with cancer. Before RC 7-8 commences, construct and write down the answers to the following questions using the information from course materials and/or internet resources.
 
#### Questions ####

- \1. Explain the concept of targeted therapy. 
    + 
- \2. Will a patient with non-small cell lung cancer with a KRAS mutation benefit from specific targeted therapy with a small molecule inhibitor directed against the EGF receptor?
    +  
- \3. Describe the **methodology of next generation sequencing (NGS; see below)** and explain why mutation detection by NGS is more sensitive than Sanger sequencing.
- \4. Unfortunately some of the diagnostic specimens are of poor quality and allow extraction of only short strands of DNA (of about 100bp). Is NGS-based sequencing be appropriate for detection of genomic aberrations in specimens with suboptimal DNA-quality? And what about the MLPA technology?
    + NGS: yes, sequencing is performed from both ends; it is very time-efficient. Normally 200bp are used to get high turnover, which is faster than longer fragments
    + _MLPA (Multiplex Ligation Dependant Probe Amplification)_: this technology is very well suited to sequence degradated strand of DNA. \(**but why??**)
- \5. What methodology is suitable for the detection of the ERBB2 gene amplification in tumor samples and explain why?
- \6. What methodology would you advise to determine loss of 1p and 19q, which is needed for the diagnosis of patients with certain brain tumors (glioma). Explain why.
- \7. Very thin tissue section (4 µm) are used for Fluorescent in situ hybridization. Explain the need for these thin sections and describe potential disadvantages.
- \8. Is array technology appropriate for detection of structural chromosomal abnormalities, such as a balanced translocation?


- - - -

### LE11-12 -- Biologics drug development ###

#### `CGRP-R`-Ab for migraine Tx show prophylactic Tx ####

|   abbr   |                    def                     |
|----------|--------------------------------------------|
| CGRP(-R) | calcitonin gene-related peptide (receptor) |
| Ab       | antibody                                   |
| hAb      | human(ized antibody)                       |
| mAb      | monoclonal antibody                        |
| hmAb     | hAb+mAb                                    |
|          |                                            |

- migraine = neuro.disorder
- migraine pathophysx involves:
    + \[CGRP]↑↑
    + also elevated in migraine attacks
- CGRP infusion induces migrain-like pain
- and Tx that lowers \[CGRP] improve migraine Sx
- hmAb against \[CGRP] or \[CGRP-R] have been developed as antimigraine pharms
- **human and humanized CGRP function blocking mAbs have demonstrated promising migraine phrophylaxis efficacy, with favorable side effect profiles in several. phase II & III studies.**
- **3 Ab's have been FDA-approved**

**CGRP-R: mechanisms of action**

![CGRP-R](./src/CGRP-R.png)

- - - -

### LE14-15 -- Immunotherapy I+II ###

<!-- 2019-09-27 @10:30--11:30|@11:30--12:30 -->

**extremely rough outline of lecture**

- Dendritic APCs are main orchestrator of the (adaptive) immune response
- PAMPs/DAMPs
- TLRs
- MHC-I/MHC-II: differences and similarities/function
- Therapy 


Balance of stimulation and inhibition of the immune system and its responses is crucial.

|      Immune stimulatory     |       Immune inhibitory       |
|-----------------------------|-------------------------------|
| TLRs                        | no antigen available          |
| co-stim molecules           | co-inhib molecules            |
| adhesion molecules          | feed-back control             |
| inflammatory cytokines      | anti-inflamm cytokines (IL-2) |
| T-cell growth factors (IL-??) | Treg                   |



**MDSCs = myeloid-derived suppressor cells**

- immature phenotype
- expand in cancer pts
- bone marrow, blood, spleen, tumor
- highly suppressive in vivo
- relate to Tx efficacy


**DC vaccination**

- many culture protocols developed (monocytes, different DC subsaets)
- DC vacc is well tolerated and safe
- clin. effective in a minority of pt
- DC maturation is crucial
- Presence of functinoal Ag-specific cells is predictive for clinical outcome
- presentation of multiple epitopes (MHC-I and MHC-II) seems beneficial
- Labour intensive

#### Additional notes: PAMPs – Toll-like Receptors ####

TLR can be categorized in subfamilies according to their PAMP-recognition:

| Lipids | nucleic acids |
|--------|---------------|
| TLR1   | TLR7          |
| TLR4   | TLR8          |
| TLR6   | TLR9          |

TLRs are expressed by various immune cells, but also on non-immune cells like fibroblasts and epithelial cells

| cell surface expression | intracellular expression |
|-------------------------|--------------------------|
| TLR1                    | TLR3                     |
| TLR2                    | TLR7                     |
| TLR4                    | TLR8                     |
| TLR5                    | TLR9                     |
| TLR6                    |                          |

NOTE: Human TLR11 is nonfunctional because of the presence of a stop codon in the gene

TLR expression is a dynamic system modulated in response to different pathogens, cytokines, environmental stresses.

Cells prominently expressing TLRs (APCs: MΦs and DCs) can activate the adaptive immune response by migrating from infection site to regional lymph node and presenting MO-derived antigens to naive CD4+ T cells. DCs also express costimulatory molecules essential to T cell activation; see below.

DCs are the central orchestrators of the various forms of immunity and tolerance; their role depends on the ligation of specific receptors, which initiate and modulate DC maturation. 
DCs can instruct differentiation of CD4+ T cells into Th1 cells (producing IFN-γ and mediating intracellular MO-defence)  or Th2 cells (producing IL-4 and IL-13, mediating extracellular MO-defence).


![bacterial cell walls](./src/bact_cell_walls.png)


Some of the unique cell wall components stimulate immune cells and serve as PAMPs, recognized by individual TLRs. Among these components, LPS is the archetype and generally the most potent immunostimulant. A lipid portion of this LPS (called 'lipid A')  is recognized by LBP (Lipid A Binding Protein, acute phase protein present in blood), binds to CD14 (GPI-linked protein on phagocyte cell surfaces), transferred to MD-2, which associated with the extracellular portion of TLR4 and finally oligomerization of the TLR4.
Different bacteria produce structurally different LPS molecules – this is reflected by the varied biological activities of lipid A.

Gram-negative bacteria carry LPS; however, lipoteichoic acid (LTA) on Gram-positive bacteria seems to function in a similar fashion as immunostimulant.
TLR2 plays a major role in detecting Gram-positive bacteria. It recognizes a variety of microbial components including: LTA, lipoproteins and PG (peptidoglycan). TLR2 interacts physically and functionally with TLR1 and TLR6, which seems to be involved in discriminationg subtle changes in the lipid portion of lipoproteins.

Flagellin (major structural protein of bacteria flagella) is a potent activator of innate immune responses; it is recognized by TLR5. TLR5 specifically recognizes the constant domain D1, a structurally central part of the flagellin protein and relatively conserved among species. TLR5 is expressed by epithelial cells, monocytes and immature DCs. TLR5 is basolaterally expressed on intestinal epithelia, so flagellin will only be recognized when invasive infection occurs, allowing commensal MOs in the gut to persist. TLR5 is also highly expressed in the lungs and seems to play an important role in pathogens of the Tr. respiratorius.

Bacterial genomic DNA is recognized by TLR9; it's immunostimulatory effect is due to unmethylated CpG dinecluotides in a particular base context designated CpG-DNA. The CpG motif is abundant in bacterial genomes; it's frequency is suppressed and it is highly methylated in mammalian cells. 
It is a very strong immunostimulant and inductor of inflammatory cytokine production and Th1 responses.
TLR9 is expressed intracellularly, in endosomal cell compartments, where acidic and reducing environment breaks down dsDNA into multiple CpG-ssDNA, which interacts directly with TLR9.


```redacted some over-detailed info, but included these two (kinda relevant) list items.
Idea to keep in mind is: mycobacteria (e.g. M. tuberculosis) is a bacterium that grows/thrives _intracellularly_
```

- TLR2 associated with TLR1 can recognize a specific secreted antigen of M. tuberculosis.
- TLR9 can be activated by mycobacterial DNA, which may be released during endolysosomal degradation

```
(... I have ommitted some information; it was way too detailed and/or medically oriented)
```

“Dendritic cells (DCs) are central in the orchestration of the various forms of immunity and tolerance. 
Their immunoregulatory role mainly relies on the ligation of specific receptors that initiate and modulate DC maturation resulting in the development of functionally different effector DC subsets that selectively promote T helper 1 (TH1)-, TH2- or regulatory T-cell (Treg)responses. 
These DC-priming receptors include pattern recognition receptors (PRRs), which discriminate between (groups of) pathogens, as well as receptors that bind tissue factors that are produced either constitutively or in response to infection with pathogens, and characterize the type of tissue and the pathogen-specific response pattern of this tissue. 
Although it is becoming increasingly clear that the selective development of T-cell-polarizing DC subsets is related to the ligation of particular receptors that are involved in DC maturation, several inconsistencies with this concept remain.” – Kapsenberg

"The different classes ofspecific immune responses are driven by the biased development of pathogen-specific effector CD4+ T-cell subsets — that is, T helper 1 (TH1) and TH2 cells, that activate different components of cellular and humoral immunity. TH1/TH2-cell-mediated immunity to microbial invasion is controlled by regulatory T cells, recently redefined as a diverse class of natural and adaptive regulatory T cells [1], that prevent autoimmunity and potentially lethal tissue destruction by chronic innate or adaptive immune responses, while they also ensure the development ofstrong immunological memory by delaying pathogen eradication." – Kapsenberg

"An important group of PRRs are Toll-like receptors (TLRs) — members ofthe IL-1 receptor (IL-1R)/TLR (IL-1R/TLR) superfamily. Adaptive immunity to pathogens often starts with the initiation of DC maturation after ligation of TLRs,which,therefore,are crucial proteins that link innate and adaptive immunity [25,26]." – Kapsenberg

Complement (divided in Classical-, Lectin-, Alternative pathways) is the first and primal immunological response to infection; thus, part of the innate immune system. It functions through enzyme cascades, eventually creating and implanting MAC on the invading MO. MAC acts as hole or tunnel in the MO's cell membrane and thereby disrupting it's internal homeostasis and killing it. Gram-negative bacteria are especially vulnerable to Complement, as they have a relatively thin LPS-layer on their surfaces (whereas Gram-positive have a relatively thick LPS-layer).

Recognition of MOs by PRRs consists of several classes of molecules:

- TLR (recognition of extracellular MOs)
- Rigl-helicases (recognition of RNA or DNA in cytoplasm); RNA or DNA should be present in the cell's nucleus, but not in the cell's cytoplasm – an indication of viral infection. Most importantly, they activate IFN-release.
- CLR; Primarily recognition of intracellular bacteria (i.e. mycobacteria) and Gram-positive bacteria.
- NOD

Three most important signals for initiation of (adaptive) immune response
1. Antigen presentation
2. Response amplitude
3. Cytokine release 
(APCs release different cytokines in response to different types of pathogens, initiation different responses)

Cytokines are extremely active molecules and are thus regulated on several levels; several different steps are needed for cytokine-release 
(LPS  MyD88  NF-kB  ProIL-1ß // PGN, uric acid  inflammasome)

- - - -

### LE16 -- CRISPR/Cas9-mediated genome editing ###

- Many bacteria possess the Cas9-gene + guiding genes; it (the synthesized enzyme) is used in their immune system to cut sequences inserted through bacteriophage infection
- Cas9 is actively being chased by scientists; detecting Cas9 in 'your favorite MO' is relevant, because these Cas9-systems are patentable.

![CRISPR/Cas9 basics](./src/CRISPR-Cas9-basic.png)

- in CRISPR/Cas9-mediated genome editing, you will inadvertedly encounter off-target effects. I.e. the genome editing is not perfect.
- you can detect unwanted effects in on-target sequences; but not in off-target sequences unless you sequence the whole genome. In this way, you will not necessarily be sure of changes you made to untargeted gene/locus/sequence.


![Cas9 variants](./src/Cas9-variants.png)

![Cas9 dsDNA break](./src/sgRNA-Cas9-dsDNA-break.png)

![T7 endonuclease I assay -- Testing CRISPR/Cas9](./src/CRISPR-Cas9-T7-endonucl-I-assay.png) 

#### CRISPR/Cas9 editing workflow ####

- \1. Design + synthesize gRNA
- \2. transfect cells
- \3. Determine freq of succesful edits
- \4. Establish single-cell clones
- \5. Screen clones for edit
- \6. characterize succesful edits

Common pitfalls:

- target site sslection
- delivery
- Cas9 activity
- off-target effects
- incidence of HDR

- - - -

### LE17 -- Introduction to tissue engineering ###

- Tissue engineering has many possible targets
    + skin
    + vessels
    + heart
    + ...

- Cell sources
    + autologous -> cells from the same patient as their implantation
    + allogeneic -> cellsfrom another individual of the same species
    + xenogeneic -> cells from another species 


| type |       source       | immune accept | offshelf avlb |
|------|--------------------|---------------|---------------|
| auto | pt.'s own cells    | yes           | no            |
| allo | other human's cell | maybe         | yes           |
| xeno | other species cell | no            | yes           |

| type |                        origin                        |
|------|------------------------------------------------------|
| ESC  | from blastocyst stage embryo (day 4-5)               |
| aSC  | present in most tissues of adult organisms           |
| iPSC | normal, adult cell reprogrammed _in vitro_ to an ESC |

#### other issues concerning the cells ####

- ethical/safety/efficacy-balance
- dev of large scale cell culture systems
- cell storage techniques
- sterilizatoin of cells
- logistics
- dev of co-culture systems
- dev of multicellular structures

- Scaffold
    + " _A biomaterial is a  mat intended to interface with biological systems to evaluate, treat, augment or replace any tissue, organ or function of the body. Scaffold can be based upon both synthetic and biological sources_ "
- Scaffold -- design parameters
    + biological/chemical composition (purity)
    + morphology (pore geometry, porosity)
    + biodegradability
    + biomech properties
    + biocompatibility
    + toxicity
    + water-binding cap
    + cosmetic aspects
    + commercial attainability
- Scaffold -- Examples
    + degradable polymers
        * PGA -- poly glycolic acid
        * PLA -- poly lactic acid (degrades to form lactate)
        * PU -- polyurethans
    + non-degr
        * PTFE -- poly(tetrafluoroethylene)
        * PET  -- polyethylene terephthalates
    + natural materials
        * proteins
        * ECM
        * chitin
        * polysaccharides

- Effector molecules
    + " _In biochemistry, an effector molecule is usually a small molecule that selectively binds to a protein to regulate its bio-activity (e.g. cell signaling). Such soluble signals include growht factors ..._ "
- Growth factor
    + ...


- general tissue engineering difficultuiers
    + multicellular arrangement
    + multiple functions of organs
    + angiogenesis
    + innervation

- - - -

### LE18 -- The extracellular matrix and scaffolds for tissue engineering ###

- Matrix mollecules influence cellular phenotype

- Cells <-> matrix
    + cell migration
    + cell-matrix interaction
    + cell proliferation
    + cell differentiation
- Microenvironment determines cell fate
    + interactions with matrix/scaffold
    + effect of soluble (signaling) molecules
    + ...

- Approaches to use ECM materials
    + decellularised tissue/organ
        * (+)  3D arch/microstruct of organ maintaied
        * (+)  generally mechanically stronger
        * (--) still (donder) organs needed
        * (--) remnants present?
    + combine isolated components into a scaffold
        * (+)  exact comp known
        * (+)  composition can be easily adapted
        * (--) what is the correct comp?
    + scaffold free (cultured cells with ECM)
        * (+) contains multiple ECM components produced by autologous cells
        * (--) 3D architecture/microstructure of organ is lost
        * (--) cells needed
        * (--) exact comp unknown

- Collagen
    + glycoproteins; >25 genetically distinct types
    + procollagen (HyPro, HyLys)
    + Gly-X-Y; many (Hy)Pro
    + Molecule triple helix, fibril formation
    + collagen provides tissue with stregth and structural integrity
    + collagen mediates biological functions like binding, migration, growth and chemotaxis of cells
    + organs: ...

- Growth factor (GF)
    + Proteins that induce specific cell behavior (pM--nM)
    + Pleiotropy
    + Redundancy
    + Effect of GF dependent on e.g.:
        * GF concentration
        * binding ability to ECM
        * ECM degradation
    + Preparation by e.g. recombinant DNA tech
    + ...
    + ...

"_combination of FGF-2 + VEGF accelerates angiogenesis + maturation of blood vessels_"

- - - -

## RC16 -- CRISPR/Cas: principles, applications and context ##

--> on brsp afterwards

- RNAi `->` RNA-DNA-hybrid; RNA is cut
- CRISPR/Cas9 `->` RNA-DNA-hybrid; DNA is cut

#### CRISPR/Cas9 vs. ZNF/TALENS ####

- CRISPR/Cas9 is:
    + \1. easy to use
    + \2. easy to adapt
    + \3. more than only a nucleas (e.g. dCas9)
    + \4. quick
    + \5. reliable
    + \6. cheap
    + \7. ss or ds break
    + \8. ..
    + \9. ..
 
 
**Aim**  
This response course is meant for asking questions, problem solving and discussing the assignments below, all related to LE 16.

**Assignments**   
The assignments refer to topics discussed in LE 16 and deal with mechanistic principles and applications of CRISPR/Cas technology in research and therapy. Similar type of questions will be part of the exam at the end of the course.

#### 1. Molecular mechanisms of CRISPR/Cas editing ####

> 1.1 Scientists initially though that the CRISPR/Cas9 system was similar to the eukaryotic RNAi silencing mechanism. Explain the crucial difference between the two.

> 1.2 In the context of antisense oligonucleotides you learned about the enzyme RNase H and its activity. Explain why the CRISPR/Cas9 enzyme is sometimes referred to as DNase H.


> 1.3. Explain the enormous success of CRISPR/Cas9 technology. Take into account a comparison with zinc finger nucleases (ZNFs) and TALENs. Give at least three arguments why CRISPR/Cas9, rather than ZFNs and TALENs, has become so popular.
 
> 1.4. The first restriction enzyme was identified in the early 1950s. Currently >3000 different restriction enzymes are known and >600 of them are commercially available. Draw a parallel between the activity and the development of restriction enzymes and that of the CRISPR/Cas9 technology. Include differences and similarities with respect to:
> - a. recognition site
> - b. enzymatic activity
> - c. use and application
 
> 1.5. What is the PAM sequence? What is it made of (DNA, RNA or protein)? What does it bind to? What is its function?

- PAM is made of DNA; it binds to the protein itself; it helps to specify the target location for Cas9 to bind to.
 
> 1.6. What are important bottle necks or weak points in CRISPR/Cas technology and how can the technology be improved? Come up with at least two original ideas.
 
#### 2. CRISPR/Cas9 application ####
 
> 2.1. Discuss the possible outcome(s) of the use of the CRISPR Cas9 system for genome editing in the case of:

You need to add the donor template ...
> - a. Cas9, one guide RNA, no donor template
- indel -> frameshift -> knockout
> - b. dCas9, one guide RNA, no donor template
- dCas9 is guided to the site; there it blocks transcription
> - c. Cas9, two guide RNAs, their targeting sequences 1 kb apart
- see notebook 
> - d. Cas9, one guide RNA with donor template
- 

- CRISPR/Cas9 cuts until there is an indel; only then will it it stop
- Cas9 will last up to 72h before it \(and the plasmid) being degraded

> 2.2. How can you prevent that CRISPR/Cas9 will cut in a recombined locus? 
 
> 2.3. Describe genome editing strategies to 
> - a. knock out a gene
> - b. remove a gene or gene fragment
> - c. remove an exon from a gene
> - d. generate a YFP fusion protein
> - e. alter promoter activity of a gene

> 2.4 Consider the genetic defect in DMD. Describe one or more therapeutic strategies for patients that carry a:
> - a. deletion of exon 43
> - b. nonsense mutation (i.e. premature stop codon) in exon 44
- the nonsense mutation could (by chance) cut out the premature stop codon; at least in a minority of the cells.
    + this strategy at least fixes some of the dystrophic muscles seen in DMD and restores at least some dystrophin in the muscle tissues
> - c. deletion of exons 45--52

> 2.5 Consider the genetic defect in SMA. Describe one or more therapeutic strategies for patients with:
> - a. two identical point mutations (>96% of the population)
> - b. two different point mutations (rare)
> - c. one point mutation and one SMN1 gene deletion (rare)

> 2.6. How would you treat patients in question 2.5 and 2.6 to obtain the required effect? Think about delivery, cell type, age of the patient, etc.

 
#### 3. Context ####

> 3.1 The CRISPR/Cas technology has been used to cure a disease in human embryos. Give three arguments in favour and three arguments against this type of developments.

> 3.2 Editing of the germline is a hot topic in risk management of CRISPR/Cas technology. Explain how a somatic (_in vivo_) genome editing procedure for a muscular dystrophy bears the risk of unintended heritable genome modification.

> 3.3 Suppose you are a member of the Nobel Prize Committee for Medicine. What an honour! Who, in your opinion, deserves to receive the Nobel Prize for the CRISPR/Cas technology and more importantly why? Note that the prize in no case will be divided among more than three persons. If you don't know the names of the scientists, simply describe their crucial contribution to the field.

- - - -

## RC13 -- Ethical aspects and scientific research ##

> **Aim**  
> If you want to conduct a study to investigate the association of a cause with an outcome using biomarkers, the biospecimens needed can be obtained (1) by prospective collection specifically for this study, (2) from a biobank, or (3) from leftover biospecimens used for diagnosis. Each way of collecting biospecimens for human research has its own legal and ethical issues to deal with. Therefore, each type of study needs its own approach. During the Response course you will discuss the different types of study and the accompanying legal and ethical issues.

- Describe the differences between specific consent, broad consent and opt-out
- If you want to collect blood and tissue specifically for a research project
    + What kind of informed consent should you use?
    + Which committee should give approval for this project?
    + What are the possibilities when there is still biomaterial available after finishing the project?
- If you want to collect blood and tissue for a 'de novo' biobank
    + What kind of informed consent should you use?
        * broad consent
    + Which committee should give approval?
        * non-WMO??
- If you want to use blood and tissue from a 'de novo' biobank for a research project
    + What kind of informed consent should be available?
        * broad consent
    + Which committee should decide whether you can start with the project?
    + What should you do before you can use biomaterial from the biobank?
        * You'll need approval from <commitee> -- you don't go back to the patient, because you've already recerived broad consent
- If you want to use leftover tissue for a research project
    + What should you do before you can use the tissue if you want to perform non-genetic analyses?
        * Get approval from IRB
    + What should you do before you can use the tissue if you want to perform genetic analyses?
        * With no chance of finding unsollicited findings; approval from IRB is sufficient. When there is a chance of finding unsollicited findings; you should include them in the application letter.
          Otherwise, you will have to go back to the patient to acquire consent.


**WMO+** 

- specific research question
- invasive / burdensome / experiment

`-> WMO obligated -> specific informed consent (opt-in) -> CMO`   

**WMO-**  

- no specific research question
- not burdensome/invasive/experiment

two options:  

- `specific informed consent (opt-in) -> IRB (CMO-light)`
- `broad informed consent (opt-in) -> IRB (CMO-light)`
    + you will need to go to the CMO-light for both:
        * initiation
        * every use

- - - -

## MIN13 RECAP ##

### material ###

- Lodish. Molecular Cell Biology. 4e.
- Alberts. Molecular Biology of the cell. 4e.
- Alberts. Essential Cell Biology. 4e.


|   ex  |        source       |                   pp                  |
|-------|---------------------|---------------------------------------|
| LE1   | Lodish              | Ch.6 pp. 251--266                     |
|       | Stryjewska 2013     | Pharmacol Rep. 65, 1086--1101         |
|       | Colosimo 2000       | BioTechniques 29, 314--331            |
| LE3   | Lodish              | Ch. 2--8, 13--16, 24                  |
|       | Hsu 2014            | Cell 157, 1262--1278                  |
|       | Festa 2013          | Proteomics 13, 1381--1399             |
| LE4   | Lodish              | Ch. 16, Ch. 21                        |
|       | Koo & Clevers 2014  | Gastroenterology 147(2);289--302      |
| LE5   | Lodish              | Ch. 4.1--4.4, Ch. 7.2, Ch. 8.1--8.4,  |
|       |                     | Ch. 13.1--13.3                        |
|       | Alberts             | Ch. 7, Ch. 8                          |
| LE6   | Alberts             | Ch. 15                                |
| LE7   | Dubbink 2014        | Molecular Oncology 8; 830--839        |
|       | de Castro 2013      | Clin Pharmacol Ther. 93; 252--259     |
|       | Lodish              | Ch. 5 (DiDeoxy sequencing of DNA)     |
|       | website `=>`        |                                       |
| LE8   | Feuk 2006           | Nat Rev Genet 7; 85--97               |
| LE9   | Naldini 2011        | Nat Rev Genet 12(5); 301--315         |
|       | Naldini 2015        | Nature 526(7573); 351--360            |
|       | Trapani 2018        | Trends Mol Med. 24, 669--681          |
| LE10  | Daya 2008           | Clin Microbiol Rev. 21(4); 583--593   |
|       | Kay 2011            | Nat Rev Genet. 12(5); 316--328        |
|       | Debyser 2003        | Curr Gene Ther. 3(6); 495--499        |
|       | website `=>`        |                                       |
| Test  | Parham              | Ch. 1, 8, 17                          |
|       | Lodish              | Ch. 23                                |
|       | Baynes              | pp. 496--512                          |
|       | Alberts             | Ch. 24, Ch. 25: Introduction to       |
|       |                     | pathogens, cell biology of infection, |
|       |                     | innate immunity                       |
| 11-12 | Lodish              | Ch. 23                                |
|       | Bologna 2013        | J Immunol 190; 231--239               |
|       | Carter 2018         | Nat Rev Drug Discov. 17(3); 197--223  |
| LE13  | website `=>`        |                                       |
|       | Nardini 2014        | Ecancermedicalscience 8; 1-9          |
|       | Riegman 2011        | Hum Genet 130; 357--368               |
|       | videos `=>`         |                                       |
| 14-15 | Lodish              | Ch. 23                                |
|       | Couzin-Frankel 2013 | Science 342; 1432--1433               |
|       | Naidoo 2014         | Br. J. Cancer, 1--6                   |
|       | Tel 2013            | Oncoimmunology 1;2(3):e23431          |
| LE16  | Komor 2017          | Cell 168, 20--36,                     |
|       |                     | PMID: 27866654                        |
|       | Sternberg 2015      | Mol. Cell 58, 568--574,               |
|       |                     | PMID: 26000842                        |
|       | website `=>`        |                                       |
| LE17  | Vacanti 1999        | Lancet;354:SI32--34                   |
|       | Berthiaume 2011     | Annu Rev Chem Biomol Eng. 2;403--30,  |
|       |                     | read pp. 403--409+421--423            |
| LE18  | Frantz 2010         | J Cell Sci 123(Pt 24);4195--200       |


